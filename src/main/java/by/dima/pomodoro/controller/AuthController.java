package by.dima.pomodoro.controller;

import by.dima.pomodoro.dto.OAuth2UrlDto;
import by.dima.pomodoro.dto.AuthOpaqueTokenDto;
import by.dima.pomodoro.service.UserService;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeRequestUrl;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeTokenRequest;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.gson.GsonFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.core.OAuth2AuthenticatedPrincipal;
import org.springframework.security.oauth2.server.resource.introspection.OpaqueTokenIntrospector;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.Arrays;

@RestController()
@RequestMapping(path = "auth")
public class AuthController {

    @Value("${spring.security.oauth2.resourceserver.opaque-token.clientId}")
    private String clientId;

    @Value("${spring.security.oauth2.resourceserver.opaque-token.clientSecret}")
    private String clientSecret;

    @Autowired
    OpaqueTokenIntrospector googleOpaqueTokenIntrospector;

    @Autowired
    UserService userService;

    @GetMapping("url")
    public ResponseEntity<OAuth2UrlDto> auth() {
        String url = new GoogleAuthorizationCodeRequestUrl(clientId,
                "http://localhost:4200",
                Arrays.asList(
                        "profile",
                        "email",
                        "openid")).build();

        return ResponseEntity.ok(new OAuth2UrlDto(url));
    }

    @GetMapping("callback")
    public ResponseEntity<AuthOpaqueTokenDto> callback(@RequestParam("code") String code) {
        String token;
        try {
            token = new GoogleAuthorizationCodeTokenRequest(
                    new NetHttpTransport(), new GsonFactory(),
                    clientId,
                    clientSecret,
                    code,
                    "http://localhost:4200"
            ).execute().getAccessToken();
        } catch (IOException e) {
            System.err.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
        OAuth2AuthenticatedPrincipal principal = googleOpaqueTokenIntrospector.introspect(token);
        userService.registerUser(principal);
        return ResponseEntity.ok(new AuthOpaqueTokenDto(token));
    }

}
