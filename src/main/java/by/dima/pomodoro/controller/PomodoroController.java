package by.dima.pomodoro.controller;

import by.dima.pomodoro.domain.Pomodoro;
import by.dima.pomodoro.repository.PomodoroRepository;
import by.dima.pomodoro.service.PomodoroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "pomodoro")
public class PomodoroController {

    @Autowired
    PomodoroService pomodoroService;

    @PostMapping
    public Pomodoro create(@RequestBody Pomodoro pomodoro) {
        return pomodoroService.create(pomodoro);
    }

}
