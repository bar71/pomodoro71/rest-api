package by.dima.pomodoro.domain;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity(name = "pomodoros")
@Getter
@Setter
@NoArgsConstructor
public class Pomodoro {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id")
    private Integer id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_sub")
    private User user;

    @Column(name = "minutes")
    private Integer minutes;

    @Override
    public String toString() {
        return "Pomodoro{" +
                "id=" + id +
                ", minutes=" + minutes +
                '}';
    }
}
