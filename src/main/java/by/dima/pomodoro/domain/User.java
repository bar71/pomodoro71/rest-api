package by.dima.pomodoro.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.api.client.util.DateTime;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.antlr.v4.runtime.misc.NotNull;
import org.springframework.lang.NonNull;
import org.springframework.security.oauth2.core.OAuth2AuthenticatedPrincipal;

import java.time.LocalDateTime;
import java.util.List;

@Entity(name = "users")
@Getter
@Setter
@NoArgsConstructor
public class User {

    @Id
    @Column(name = "sub")
    private String sub;

    @Column(name = "name")
    private String name;

    @Column(name = "email")
    private String email;

    @Column(name = "picture")
    private String picture;

    @Column(name = "last_auth_datetime")
    private LocalDateTime lastAuthDateTime;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "user")
    @JsonIgnore
    private List<Pomodoro> pomodoros;

    public User(@NonNull OAuth2AuthenticatedPrincipal principal) {
        this.sub = principal.getAttribute("sub");
        this.name = principal.getAttribute("name");
        this.email = principal.getAttribute("email");
        this.picture = principal.getAttribute("picture");
    }

    @Override
    public String toString() {
        return "User{" +
                "sub=" + sub +
                ", username='" + name + '\'' +
                '}';
    }
}
