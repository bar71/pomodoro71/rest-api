package by.dima.pomodoro.dto;

public record AuthOpaqueTokenDto(String opaqueToken) {
}
