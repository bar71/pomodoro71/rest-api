package by.dima.pomodoro.dto;

public record GoogleUserInfo(
        String sub,
        String name,
        String email,
        String picture
) {
}
