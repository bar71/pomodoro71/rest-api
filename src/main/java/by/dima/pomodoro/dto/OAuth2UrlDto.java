package by.dima.pomodoro.dto;

public record OAuth2UrlDto(String url) {
}
