package by.dima.pomodoro.repository;

import by.dima.pomodoro.domain.Pomodoro;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PomodoroRepository extends JpaRepository<Pomodoro, Integer> {
}
