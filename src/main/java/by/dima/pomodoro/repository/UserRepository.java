package by.dima.pomodoro.repository;

import by.dima.pomodoro.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, String> {
}
