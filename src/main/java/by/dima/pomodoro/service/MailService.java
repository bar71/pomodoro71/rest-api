package by.dima.pomodoro.service;

import by.dima.pomodoro.domain.User;

public interface MailService {

    void sendAuthEmail(String toEmail, User user);

}
