package by.dima.pomodoro.service;

import by.dima.pomodoro.domain.Pomodoro;

public interface PomodoroService {

    Pomodoro create(Pomodoro pomodoro);

}
