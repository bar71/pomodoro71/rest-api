package by.dima.pomodoro.service;

import by.dima.pomodoro.domain.User;
import org.springframework.security.oauth2.core.OAuth2AuthenticatedPrincipal;

public interface UserService {

    User registerUser(OAuth2AuthenticatedPrincipal principal);

}
