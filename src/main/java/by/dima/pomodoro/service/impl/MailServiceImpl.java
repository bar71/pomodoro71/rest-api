package by.dima.pomodoro.service.impl;

import by.dima.pomodoro.domain.User;
import by.dima.pomodoro.service.MailService;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

@Service
public class MailServiceImpl implements MailService {

    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private Configuration config;

    @Value("${spring.mail.username}")
    private String fromEmail;

    private final String AUTH_EMAIL_TEMPLATE = "auth.ftl";

    private final String AUTH_SUBJECT = "Авторизация в Pomodoro71";

    @Override
    public void sendAuthEmail(String toEmail, User user) {
        MimeMessage message = mailSender.createMimeMessage();
        try {
            MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                    StandardCharsets.UTF_8.name());

            Template t = config.getTemplate(AUTH_EMAIL_TEMPLATE);
            String html = FreeMarkerTemplateUtils.processTemplateIntoString(t, user);

            helper.setTo(toEmail);
            helper.setText(html, true);
            helper.setSubject(AUTH_SUBJECT);
            helper.setFrom(fromEmail);
            Thread thread = new Thread(() -> mailSender.send(message));
            thread.start();
        } catch (IOException | TemplateException | MessagingException e) {
            System.out.println(e.getMessage());
        }
    }



}
