package by.dima.pomodoro.service.impl;

import by.dima.pomodoro.domain.Pomodoro;
import by.dima.pomodoro.repository.PomodoroRepository;
import by.dima.pomodoro.service.PomodoroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PomodoroServiceImpl implements PomodoroService {

    @Autowired
    PomodoroRepository pomodoroRepository;

    @Override
    public Pomodoro create(Pomodoro pomodoro) {
        return pomodoroRepository.save(pomodoro);
    }

}
