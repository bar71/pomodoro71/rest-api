package by.dima.pomodoro.service.impl;

import by.dima.pomodoro.domain.User;
import by.dima.pomodoro.repository.UserRepository;
import by.dima.pomodoro.service.MailService;
import by.dima.pomodoro.service.UserService;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.security.oauth2.core.OAuth2AuthenticatedPrincipal;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    MailService mailService;

    @Override
    public User registerUser(@NonNull OAuth2AuthenticatedPrincipal principal) {
        Optional<User> userOptional = userRepository.findById(principal.getAttribute("sub"));
        User user;
        if (userOptional.isPresent()) {
            user = userOptional.get();
        } else {
            user = userRepository.save(new User(principal));
        }
        user.setLastAuthDateTime(LocalDateTime.now());
        mailService.sendAuthEmail(user.getEmail(), user);
        return user;
    }

}
