<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Pomodoro71</title>
    <style>
        body {
            font-family: "Roboto Light";
        }
        hr {
            color: black;
            background-color: black;
        }
        .info {
            background-color: bisque;
            border: solid 2px black;
            font-size: 18px;
            padding: 20px;
        }
    </style>
</head>
<body>
<div class="main">
    <h2>Уважаемый ${name}, Вы авторизовались на сервисе через Google.</h2>

    <div class="info">
        Дата и время авторизации: ${lastAuthDateTime.format("dd.MM.yyyy HH:MM")}.
    </div>

    <footer>
        <h3>Благодарим за пользование сервисом POMODORO71</h3>
    </footer>
</div>
</div>
</body>
</html>
