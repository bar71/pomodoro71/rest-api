package by.dima.pomodoro;

import by.dima.pomodoro.domain.Pomodoro;
import by.dima.pomodoro.domain.User;
import by.dima.pomodoro.repository.PomodoroRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@SpringBootTest
class PomodoroApplicationTests {

	@Autowired
	PomodoroRepository pomodoroRepository;

	@Test
	@Transactional
	void contextLoads() {
		List<Pomodoro> all = pomodoroRepository.findAll();
		User user = all.get(0).getUser();
		System.out.println(user.getUsername());
		System.out.println("");
	}

}
